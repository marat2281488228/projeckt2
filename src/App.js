import React, {useState} from 'react';
import './App.css';
import StartWin from './views/StartWin';
import GameWin from './views/GameWin';

function App() {

  const[settings, setSettings] = useState({
    theme: 'Светлая'
  })

  return (
    <div className="App">
      <StartWin settings={settings} setSettings={setSettings} />
      <GameWin />
    </div>
  );
}

export default App;
