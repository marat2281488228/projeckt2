import React from 'react';
import './Settings.css';

function Settings({setIsSettingWin, settings, setSettings}) {

  console.log(settings)
  
  function saveSettings(){
    let select = document.getElementById('theme').value
    let data = {
      theme: select
    }
    setSettings(data)
  }

  return (
  <>
    <div className='echo' onClick={() => setIsSettingWin(false)}></div>
    <div className="Settings">
        <h1>Настройки</h1>
        <label>Выберите тему</label>
        <select id='theme'>
          <option>Светлая</option>
          <option>Темная</option>
        </select>
        <button onClick={saveSettings}>Сохранить</button>
    </div>
  </>
  );
}

export default Settings;
