import React, { useState } from 'react';
import './StartWin.css';
import Settings from '../components/Settings';

function StartWin({settings, setSettings}) {

  const[isSettingWin, setIsSettingWin] = useState(false)

  return (
    <div className="StartWin">
      <button>Начать игру</button>
      <button onClick={ () => setIsSettingWin(true) }>Настройки</button>
      { isSettingWin ? <Settings setIsSettingWin={setIsSettingWin} settings={settings} setSettings={setSettings} /> : null }
    </div>
  );
}

export default StartWin;

